import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;

import lejos.nxt.Button;
import lejos.nxt.ButtonListener;
import lejos.nxt.LCD;
import lejos.nxt.Motor;
import lejos.nxt.SensorPort;
import lejos.nxt.SensorPortListener;
import lejos.nxt.Sound;
import lejos.nxt.comm.BTConnection;
import lejos.nxt.comm.Bluetooth;
import lejos.nxt.comm.NXTConnection;


public class NXTController {
	
	private static final char END = 'e';
	private static final char FORWARD = 'f';
	private static final char BACKWARD = 'b';
	private static final char TURN ='t';
	private static final char CHANGE_SPEED = 'c';
	private static final char START_GAME = 'g';
	private static final char STOP = 's';
	private static final char HIT = 'h';
	private static final char DEF = 'd';
	private static final int RECOVERY_TIME = 3000;
	private static final int HEALTH = 10;
	
	private boolean isForward;
	private boolean isBackward;
	private DataInputStream inputStream;
	private DataOutputStream outputStream;
	private BTConnection connection;
	private long lastHitTime;
	private int numberOfHits;
	private boolean gameStarted;
	
	
	private class ExitListener implements ButtonListener {
		public void buttonPressed(Button b) {
			if (connection != null) {
				connection.close();
			}
			System.exit(0);
		}

		public void buttonReleased(Button b) {}
	}
	
	
	private class TouchListener implements SensorPortListener {
		private int oldValue;
		private DataOutputStream outputStream;
		
		public TouchListener(DataOutputStream outputStream) {
			oldValue = -1;
			this.outputStream = outputStream;
		}
		
		public void stateChanged(SensorPort aSource, int aOldValue, int aNewValue) {
			if ((oldValue == -1) || (!gameStarted)) {
				oldValue = aNewValue;
				return;
			}
			
			long currentTime = System.currentTimeMillis();
			if (currentTime - lastHitTime >= RECOVERY_TIME) {
				// TODO przeploty?
				lastHitTime = currentTime;
				
				try {
					outputStream.writeChar(HIT);
					outputStream.flush();
				} catch (IOException e) {
					LCD.drawString(e.getMessage(), 0, 0);
				}
				
				Sound.playSample(new File("headshot.wav"), 100);
				
				numberOfHits++;
				if (numberOfHits >= HEALTH) {
					gameStarted = false;
					LCD.drawString("Loser!", 0, 0);
					Sound.playSample(new File("PacManDies.wav"), 100);
				}
			}
		}
	}
	
	
	public NXTController() {
		isForward = false;
		isBackward = false;
		lastHitTime = 0;
		numberOfHits = 0;
		gameStarted = false;
		Button.ENTER.addButtonListener(new ExitListener());
	}
	
	public void connect() {
		LCD.drawString("Connecting...", 0, 0);
		connection = Bluetooth.waitForConnection(0, NXTConnection.RAW);
		inputStream = connection.openDataInputStream();
		outputStream = connection.openDataOutputStream();
		SensorPort.S1.addSensorPortListener(new TouchListener(outputStream));
		SensorPort.S2.addSensorPortListener(new TouchListener(outputStream));
		LCD.clear();
	}
	
	public void disconnect() {
		connection.close();
	}
	
	private void setSpeed(int speed) {
		Motor.B.setSpeed(speed);
		Motor.C.setSpeed(speed);
	}
	
	private void goForward() {
		if (!isForward) {
			isForward = true;
			isBackward = false;
			Motor.B.forward();
			Motor.C.forward();
		}
	}
	
	private void goBackward() {
		if (!isBackward) {
			isBackward = true;
			isForward = false;
			Motor.B.backward();
			Motor.C.backward();
		}
	}
	
	private void stop() {
		if (isBackward || isForward) {
			isBackward = false;
			isForward = false;
			Motor.B.stop(true);
			Motor.C.stop(true);
			Motor.A.stop();
		}
	}
	
	private void changeDirection(int value) {
		Motor.A.rotateTo(value, true);
	}
	
	private void startGame() {
		numberOfHits = 0;
		gameStarted = true;
		LCD.clear();
	}
	
	public void run() {
		char operation;
		int value = 0;
		boolean end = false;
		while (!end) {
			operation = DEF;
			try {
				operation = inputStream.readChar();
				value = inputStream.readInt();
			}
			catch(IOException e) {
				LCD.drawString(e.getMessage(), 0, 0);
				end = true;
			}
			switch(operation) {
				case FORWARD:
					goForward();
					break;
				case BACKWARD:
					goBackward();
					break;
				case TURN:
					changeDirection(value);
					break;
				case CHANGE_SPEED:
					setSpeed(value);
					break;
				case STOP:
					stop();
					break;
				case START_GAME:
					startGame();
					break;
				case END:
					end = true;
					break;
				default:
					break;
			}
		}
	}
	
	public static void main(String[] args) {
		NXTController controller = new NXTController();
		controller.connect();
		controller.run();
		controller.disconnect();
	}
}
